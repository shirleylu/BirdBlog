<!DOCTYPE HTML>
<html>
<head>  
    <meta charset="utf-8">  
    <title>发表文章</title>
    <script type="text/javascript" src="../js/common.js"></script>
    <script type="text/javascript" src="../lib/mdeditor.js"></script>
    <link rel="stylesheet" type="text/css" href="../css/common.css">
    <link rel="stylesheet" type="text/css" href="../css/publish.css">
</head>
<body>
    <div id="header">
        <div id="left">
            <img src="../res/logo.png" />
            <input type="text" placeholder="请输入文章标题" />
        </div>
        <div id="right">
            <button id="draft">保存草稿</button>
            <button id="publish" class="mainbutton">发表</button>
        </div>
    </div>
    <div id="main" class="divshadow">
        <textarea id="source" ></textarea>
        <div id="separate"></div>
        <div id="mark" ></div>
    </div>
</body>
    <script type="text/javascript" src="../js/publish.js"></script>
</html>
<!DOCTYPE HTML>
<html>
<head>  
    <meta charset="utf-8">  
    <title>初始化BirdBlog程序</title>
    <link rel="stylesheet" type="text/css" href="../../css/common.css">
    <link rel="stylesheet" type="text/css" href="../../css/install.css">
</head>
<body onload="load()">

    <div id="out" class="winshadow">
        <div id="header">
            <img src="../../res/logo.png" alt="BirdBlog"/>
        </div>
        <div id="main">

            <?php
                // 关闭错误报告
                //error_reporting(0);

                // 防止伪造提交
                $servername = $_SERVER['SERVER_NAME'];      // 域名
                if(substr($_SERVER["HTTP_REFERER"], 7, strlen($servername)) != $servername)
                {
                    echo "<script>window.location.href='install.php'</script>";
                }

                $action = isset($_GET['action']);           // 检测是否存在提交action
                if ($action != "install")                   // 如果没有提交，则进入安装界面
                {
            ?>

                <form id="install" onsubmit="return formcheck(1, 0)" action="install.php?action=install" method="POST">
                    <div id="database-address" class="main-item">
                        <div class="top">
                            <h4>数据库地址：</h4>
                            <input type="text" name="database-address" id="database-address-input"/>
                        </div>
                        <div id="database-address-tip" class="tip">* 本地数据库为localhost，或运营商提供的地址</div>
                    </div>
                    <div id="database-name" class="main-item">
                        <div class="top">
                            <h4>数据库名称：</h4>
                            <input type="text" name="database-name" id="database-name-input"/>
                        </div>
                        <div id="database-name-tip" class="tip"></div>
                    </div>
                    <div id="database-user" class="main-item">
                        <div class="top">
                            <h4>数据库用户：</h4>
                            <input type="text" name="database-user" id="database-user-input"/>
                        </div>
                        <div id="database-user-tip" class="tip"></div>
                    </div>
                    <div id="database-password" class="main-item">
                        <div class="top">
                            <h4>数据库密码：</h4>
                            <input type="password" name="database-password" id="database-password-input"/>
                        </div>
                        <div id="database-password-tip" class="tip"></div>
                    </div>
                    <div id="address-prefix" class="main-item">
                        <div class="top">
                            <h4>数据表前缀：</h4>
                            <input type="text" name="database-prefix" id="database-prefix-input"/>
                        </div>
                        <div id="database-prefix-tip" class="tip">* 数据表前缀，建议不用修改</div>
                    </div>
                    <div id="login-user" class="main-item">
                        <div class="top">
                            <h4>登录用户名：</h4>
                            <input type="text" name="login-user" id="login-user-input"/>
                        </div>
                        <div id="login-user-tip" class="tip">* 根据自己习惯设定</div>
                    </div>
                    <div id="login-password" class="main-item">
                        <div class="top">
                            <h4>登 录 密 码：</h4>
                            <input type="password" name="login-password" id="login-password-input"/>
                        </div>
                        <div id="login-password-tip" class="tip">* 根据自己习惯设定</div>
                    </div>

                    <input id="action" type="submit" name="submit" value="开  始  安  装"/>
                </form>

            <?php
                }
                else        // 存在提交时，登录、初始化
                {
                    include "../../lib/mysql.php";

                    $dbhost = $_POST["database-address"];           // 数据库地址
                    $dbname = $_POST["database-name"];              // 数据库名称
                    $dbuser = $_POST["database-user"];              // 数据库用户名
                    $dbpass = $_POST["database-password"];          // 数据库密码
                    $dbprefix = $_POST["database-prefix"];          // 数据表前缀
                    $loginuser = $_POST["login-user"];              // 登录用户名
                    $loginpass = $_POST["login-password"];          // 登录密码

                    $connect = Mysql::Instance()->Connect($dbhost, $dbname, $dbuser, $dbpass);      // 连接数据库

            ?>
                <div id="comment">
            <?php

                    if ($connect == 1)      // 数据库连接成功
                    {
                        echo "MySQL连接成功"."<br/>";
                        echo "选择数据库".$dbname."成功"."<br/>";

                        Mysql::Instance()->Query("create table user
                        (
                            id int primary key not null auto_increment comment '用户ID',
                            name char(50) not null comment '用户名',
                            password char(200) not null comment '密码',
                            email char(200) comment '电子邮箱',
                            login bool default false comment '登录状态'
                        );");

                        echo "创建用户数据表成功"."<br/>";

                        $passwordHash = password_hash($loginpass, PASSWORD_DEFAULT);
                        Mysql::Instance()->Query("insert into user (name, password) values ('".$loginuser."','".$passwordHash."');");

                        Mysql::Instance()->Query("create table article
                        (
                            id int primary key not null auto_increment comment '文章ID',    
                            title char(50) not null comment '标题',
                            userid int not null comment '用户ID',
                            date date not null comment '日期',
                            time datetime not null comment '时间',
                            content text not null comment '内容',
                            allowcomment bool not null comment '是否允许评论',
                            isshow bool not null comment '是否显示',    
                            visits int comment '观看数',
                            likes int comment '点赞数'
                        );");

                        echo "创建文章数据表成功"."<br/>";
                        

                        // 把数据库信息写入配置文件
                        $config = fopen("../config.php", "w") or die("Unable to open file!");
                        fwrite($config, "<?php\n");
                        fwrite($config, "\t\$dbhost = '".$dbhost."';\n");
                        fwrite($config, "\t\$dbname = '".$dbname."';\n");
                        fwrite($config, "\t\$dbuser = '".$dbuser."';\n");
                        fwrite($config, "\t\$dbpassword = '".$dbpass."';\n");
                        fwrite($config, "?>");
                        fclose($config);
            ?>
                </div>
                <div id="jump">
                    <a>恭喜您，birdblog博客程序安装成功！</a><br/><br/>
                    <button onclick="jumpto('../../main/init.php')">转到首页</button>
                    <button onclick="jumpto('../admin.php')">登录后台</button>            
                </div>
            <?php
                    }
                    else                    // 数据库连接失败
                    {
                        echo "连接失败";
                        $errid = Mysql::Instance()->GetErrID();
                        echo "<script>";
                        echo "formcheck($connect, $errid);";
                        echo "</script>";
                    }
                }
            ?> 
                </div>
        </div>
    </div>
</body>
    <script type="text/javascript" src="../../js/install.js"></script>
</html>
create table article
(
    id int primary key not null auto_increment comment '文章ID',    
    title char(50) not null comment '标题',
    userid int not null comment '用户ID',
    date date not null comment '日期',
    time datetime not null comment '时间',
    content text not null comment '内容',
    allowcomment bool not null comment '是否允许评论',
    isshow bool not null comment '是否显示',    
    visits int comment '观看数',
    likes int comment '点赞数'
);

create table user
(
    id int primary key not null auto_increment comment '用户ID',
    name char(50) not null comment '用户名',
    password char(200) not null comment '密码',
    email char(200) not null comment '电子邮箱'    
);
<?php

class Dispather
{
    function __construct()
    {
        if (is_readable("./birdblog/admin/install/install.php"))
        {
            header("Location: ./birdblog/admin/install/install.php");
        }
        else
        {
            header("Location: /birdblog/main/init.php");
        }
    }

    function __destruct()
    {
        
    }
}
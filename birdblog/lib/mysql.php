<?php

class Mysql
{
    private $dbhost = "";
    private $dbname = "";
    private $dbuser = "";
    private $dbpassword = "";
    private $connect = null;

    public static $instance;

    function __construct()
    {

    }

    function __destruct()
    {
        if ($this->connect != null)
        {
            mysqli_close($this->connect);         
        }
    }    
    
    public static function Instance()
    {
        if(!(self::$instance instanceof self))
        {
            self::$instance = new self;
        }
        return self::$instance;
    }

    public function Connect($dbhost, $dbname, $dbuser, $dbpass)
    {
        $this->dbhost = $dbhost;
        $this->dbname = $dbname;
        $this->dbuser = $dbuser;
        $this->dbpassword = $dbpass;

        $this->connect = mysqli_connect($dbhost, $dbuser, $dbpass, $dbname); // 连接
        if (!$this->connect)  return -1;

        $db = mysqli_select_db($this->connect, $dbname);            // 选择数据库
        if (!$db) return -2;

        $this->Query("set names utf8;");                            // 设置编码

        return 1;
    }

    public function Query($sql)
    {
        $query = mysqli_query($this->connect, $sql);
        if (!$query) return -1;

        return $query;
    }

    public function GetErr()
    {
        return mysqli_error($this->connect);
    }

    public function GetErrID()
    {
        return mysqli_errno($this->connect);
    }






    /*
    报错函数
    */
    /*function err($error)
    {
        die('对不起，您的操作有误，错误原因为'.$error);//die相当于echo和exit的组合
    }*/

    /*
    配置数组array($dbhost,$dbuser,$dbpsw,$dbname,$dbcharset)
    */
    /*function connect($config)
    {
        extract($config);
        if(!($con=mysql_connect($dbhost,$dbuser,$dbpsw)))
        {
            $this->err(mysql_error());
        }
        if(!mysql_select_db($dbname,$con))
        {//选库
            $this->err(mysql_error());
        }
        mysql_query('set names '.$dbcharset);//设置编码格式
    }

    function query($sql)
    {
        if(!($query=mysql_query($sql)))
        {
            $this->err($sql.'<br>'.mysql_error());
        }
        else
        {
            return $query;
        }
    }

    function findAll($query)
    {
        while($rs=mysql_fetch_array($query,MYSQL_ASSOC))
        {
            $list[]=$rs;
        }
        return isset($list)?$list:'';
    }

    function findOne($query)
    {
        $rs=mysql_fetch_array($query,MYSQL_ASSOC);
        return $rs;
    }

    function findResult($query,$row=0,$field=0)
    {
        $rs=mysql_result($query,$row,$field);
        return $rs;
    }
    function insert($table,$arr)
    {
        //$sql="INSERT INTO 表名 values";
        foreach($arr as $key=>$value)
        {
            $value=mysql_real_escape_string($value);
            $keyArr[]='`'.$key.'`';
            $valueArr[]='`'.$value.'`';
        }
        $keys=implode(',',$keyArr);
        $values=implode(',',$valueArr);
        $sql='INSERT INTO '.$table.'('.$keys.')VALUES('.$values.')';
        $this->query($sql);
        return mysql_insert_id();
    }

    function update($table,$arr,$where)
    {
        foreach($arr as$key=>$value)
        {
            $value=mysql_real_escape_string($value);
            $keyAndvalueArr[]='`'.$key.'`=`'.$value.'`';
        }
        $keyAndvalues=implode(',',$keyAndvalueArr);
        $sql='UPDATE '.$table.'SET'.$keyAndvalues.' WHERE '.$where;
        $this->query($sql);
    }

    function del($table,$where)
    {
        $sql='DELETE FROM '.$table.' WHERE '.$where;
        $this->query($sql);
    }*/
    
 }
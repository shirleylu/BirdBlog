function $(id)
{
	return document.getElementById(id);
}

function load()
{
	if ($("database-address-input"))
	{
		$("database-address-input").value = "localhost";
	}

	if ($("database-prefix-input"))
	{
		$("database-prefix-input").value = "birdblog_";
	}	
}

function formcheck(id, err)
{
	tipInit();

	var dbadr = $("database-address-input").value;
	if(dbadr == "")
	{
		$("database-address-tip").innerText = "* 数据库地址不能为空";
		return false;
	}

	var dbname = $("database-name-input").value;
	if (dbname == "")
	{
		$("database-name-tip").innerText = "* 数据库名不能为空";
		return false;
	}

	var dbuser = $("database-user-input").value;
	if (dbuser == "")
	{
		$("database-user-tip").innerText = "* 数据库用户名不能为空";
		return false;
	}

	var dbpass = $("database-password-input").value;
	if (dbpass == "")
	{
		$("database-password-tip").innerText = "* 数据库密码不能为空";
		return false;
	}

	var dbpre = $("database-prefix-input").value;
	if (dbpre == "")
	{
		$("database-prefix-tip").innerText = "* 数据库前缀不能为空";
		return false;
	}

	var loginuser = $("login-user-input").value;
	if (loginuser == "")
	{
		$("login-user-tip").innerText = "* 登录用户名不能为空";
		return false;
	}

	var loginpass = $("login-password-input").value;
	if (loginpass == "")
	{
		$("login-password-tip").innerText = "* 登录密码不能为空";
		return false;
	}

	if (id == -1 && err == 2005)
	{
		$("database-address-tip").innerText = "* 数据库地址错误或数据库服务器不可用";
		return false;
	}
	else if (id == -1 && err == 2003)
	{
		$("database-address-tip").innerText = "* 数据库地址端口错误";
		return false;		
	}
	else if (id == -1 && err == 2006)
	{
		$("database-address-tip").innerText = "* 数据库服务器不可用";
		return false;		
	}
	else if (id == -1 && err == 1045)
	{
		$("database-user-tip").innerText = "* 数据库用户名或密码错误";
		$("database-password-tip").innerText = "* 数据库用户名或密码错误";
		return false;
	}
	else if (id == -1)
	{
		$("database-address-tip").innerText = "* 连接数据库失败，错误码：" + err;
		return false;
	}
	else if (id == -2)
	{
		$("database-name-tip").innerText = "* 数据库名称填写错误";
		return false;
	}

	return true;
}

function tipInit()
{
	$("database-address-tip").innerText = "* 本地数据库为localhost，或运营提供的地址";
	$("database-name-tip").innerText = "";
	$("database-user-tip").innerText = "";
	$("database-password-tip").innerText = "";
	$("database-prefix-tip").innerText = "* 数据表前缀，建议不用修改";
	$("login-user-tip").innerText = "* 根据自己习惯设定";
	$("login-password-tip").innerText = "* 根据自己习惯设定";
}

function jumpto(url)
{
	window.location.href = url;
}